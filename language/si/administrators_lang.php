<?php

$lang['administrators_app_description'] = 'මෙම පරිපාලකයින් යෙදුම සමඟ, ඔබ පද්ධතිය භාවිතා කරන්නන් කණ්ඩායම් විශේෂිත යෙදුම් වෙත පිවිසුම් ලබා ගත හැක.';
$lang['administrators_app_name'] = 'පරිපාලකවරුනි';
