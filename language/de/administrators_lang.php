<?php

$lang['administrators_app_description'] = 'Mit der Administrator-App können Sie den Zugriff auf bestimmte Anwendungen zu Gruppen von Benutzern auf das System gewähren.';
$lang['administrators_app_name'] = 'Administratoren';
